#!/bin/bash

image_name=${1}

#echo "Image_name=${image_name}"

token=$(curl -s -H 'GET' "https://auth.docker.io/token?service=registry.docker.io&scope=repository:${image_name}:pull" | jq -r '.token')

#echo "Token: ${token}"

tags=$(curl -s --request 'GET' -H "Authorization: Bearer ${token}" "https://registry-1.docker.io/v2/${image_name}/tags/list")

#echo "Tags: "
echo "${tags}"